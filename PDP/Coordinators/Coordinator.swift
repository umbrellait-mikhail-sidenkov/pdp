//
//  Coordinator.swift
//  PDP
//
//  Created by Mikhail Sidenkov on 23.12.2021.
//

import Foundation
import UIKit
import Swinject

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navController: UINavigationController { get set }
    
    func start()
}

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navController: UINavigationController

    private let container: Container

    init(navController: UINavigationController, container: Container) {
        self.navController = navController
        self.container = container
    }
    
    func start() {
        let vc = container.resolve(MainViewController.self, argument: self)!
        navController.pushViewController(vc, animated: false)
    }

    func viewPhoto(_ photo: UnsplashPhoto) {
        let vc = container.resolve(PhotoVC.self, arguments: self, photo)!
        navController.present(vc, animated: true)
        print("navstack: \(navController.viewControllers)")
    }
}
