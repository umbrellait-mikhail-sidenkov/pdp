//
// Created by Mikhail Sidenkov on 23.12.2021.
//

import UIKit

class PhotoVC: UIViewController {
    let view_: PhotoView
    weak var coordinator: MainCoordinator?

    let photo: UnsplashPhoto
    let imageRepo: ImageRepository

    init(_ coordinator: MainCoordinator, photo: UnsplashPhoto, imageRepo: ImageRepository) {
        view_ = PhotoView()
        self.imageRepo = imageRepo
        self.coordinator = coordinator
        self.photo = photo
        super.init(nibName: nil, bundle: nil)
        title = "Photo"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = view_
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        imageRepo.getImageBy(url: photo.regularUrl) { [weak self] image in
            DispatchQueue.main.async {
                self?.view_.photo.image = image
            }
        }
    }
}
