//
// Created by Mikhail Sidenkov on 23.12.2021.
//

import UIKit

class PhotoView: UIView {
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .black

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let photo = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }

    func setupSubviews() {
        addSubview(photo)

        photo.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
