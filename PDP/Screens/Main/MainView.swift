//
//  MainView.swift
//  PDP
//
//  Created by Mikhail Sidenkov on 16.12.2021.
//

import UIKit
import SnapKit
import Then
import Combine

enum MainViewSection {
    case main
}

class MainView: UIView {
    typealias Snapshot = NSDiffableDataSourceSnapshot<MainViewSection, UnsplashPhoto>
    typealias DiffableDataSource = UICollectionViewDiffableDataSource<MainViewSection, UnsplashPhoto>

    private var cancellables: [AnyCancellable] = []
    private var openPhoto: ((UnsplashPhoto) -> ())? = nil

    private let imageRepo: ImageRepository

    private var photos: [UnsplashPhoto] = [] {
        didSet {
            var snapshot = Snapshot()
            snapshot.appendSections([.main])
            snapshot.appendItems(photos)
            dataSource.apply(snapshot)
        }
    }

    // todo: maybe move out everything repo related to VC?
    init(imageRepo: ImageRepository, onPhotoSelection: ((UnsplashPhoto) -> ())?) {
        self.imageRepo = imageRepo
        openPhoto = onPhotoSelection

        super.init(frame: .zero)
        backgroundColor = .systemBackground

        setupSubviews()
        setupConstraints()
        setupBindings()
        setupCollectionView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupCollectionView() {
        collectionView.prefetchDataSource = self
        collectionView.delegate = self
        collectionView.register(PhotoCollectionCell.self,
                                forCellWithReuseIdentifier: PhotoCollectionCell.cellId)
    }

    lazy var dataSource = DiffableDataSource(
        collectionView: collectionView
    ) { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
        guard let self = self else { return UICollectionViewCell() }

//        print("self.photos.count - 1 = \(self.photos.count - 1)")
//        print("self.photos.count - 10 = \(self.photos.count - 10)")
//        print("indexPath.row = \(indexPath.row)")
        if self.photos.count - 1 == indexPath.row && !self.photos.isEmpty {
            store.dispatch(.loadNextPage)
//            print("dispatch")
        }
//        print()

        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: PhotoCollectionCell.cellId,
            for: indexPath
        ) as! PhotoCollectionCell

//        cell.configure(url: item.url)

        let url = item.thumbUrl
        cell.currentPhotoUrl = url

        if let image = self.imageRepo.getCachedImage(url: url) {
            cell.photoView.image = image
        } else {
            cell.photoView.image = nil

            self.imageRepo.fetchAsync(url: url) { fetchedData in
                DispatchQueue.main.async {
                    guard cell.currentPhotoUrl == url else { return }
                    cell.photoView.image = fetchedData
                }
            }
        }

        return cell
    }

    private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()

    private func setupSubviews() {
        addSubview(collectionView)
    }

    private func setupConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(snp.bottom)
        }
    }

    private func setupBindings() {
        store.statePublisher.sink { [weak self] in
            self?.photos = $0.photos
        }.store(in: &cancellables)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let availableWidth = collectionView.bounds.inset(by: collectionView.layoutMargins).width - 2
//        print("UIScreen.main.bounds.width = \(UIScreen.main.bounds.width)")
//        print("availableWidth = \(availableWidth)")
        let cellSide = (availableWidth / 3)
//        print("cellSide = \(cellSide)")
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: cellSide, height: cellSide)
        }
    }
}

extension MainView: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openPhoto?(photos[indexPath.row])
    }
}

extension MainView: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        print("prefetch = ")
        for indexPath in indexPaths {
            imageRepo.fetchAsync(url: photos[indexPath.row].thumbUrl)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
//        print("cancel prefetch = ")
        for indexPath in indexPaths {
            imageRepo.cancelFetch(photos[indexPath.row].thumbUrl)
        }
    }
}
