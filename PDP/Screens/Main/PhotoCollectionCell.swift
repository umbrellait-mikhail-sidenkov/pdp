//
// Created by Mikhail Sidenkov on 20.12.2021.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    static let cellId = "PhotoCollectionCell"

    var currentPhotoUrl = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(photoView)
        makeConstraints()
        backgroundColor = .systemGray6
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let photoView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }

    private func makeConstraints() {
        photoView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        photoView.image = nil
    }
}