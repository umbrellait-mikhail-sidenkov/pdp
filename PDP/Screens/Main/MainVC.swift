//
//  ViewController.swift
//  PDP
//
//  Created by Mikhail Sidenkov on 16.12.2021.
//

import UIKit

class MainViewController: UIViewController {

    let view_: MainView
    weak var coordinator: MainCoordinator?
    
    init(_ coordinator: MainCoordinator, imageRepo: ImageRepository) {
        view_ = MainView(
            imageRepo: imageRepo,
            onPhotoSelection: { photo in
                coordinator.viewPhoto(photo)
            }
        )

        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
        title = "Main"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = view_
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        store.dispatch(.loadNextPage)
    }
}
