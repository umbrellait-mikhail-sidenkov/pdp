//
// Created by Mikhail Sidenkov on 16.12.2021.
//

import Foundation
import Combine
import CombineRex

struct AppState {
    var photos: [UnsplashPhoto] = []
    var photosPage = 1
}

enum AppAction {
    case clearPhotos,
         loadNextPage,
         addPhotos(photos: [UnsplashPhoto])
}

let appReducer = Reducer<AppAction, AppState>.reduce { action, state in
    switch action {
    case .clearPhotos:
        state.photos = []
        state.photosPage = 1
    case .loadNextPage:
        state.photosPage += 1
    case let .addPhotos(photos):
        state.photos.append(contentsOf: photos)
    }
}

let store = ReduxStoreBase<AppAction, AppState>(
    subject: .combine(initialValue: AppState()),
    reducer: appReducer,
    middleware: FetchPhotosMiddleware()
)


final class FetchPhotosMiddleware: MiddlewareProtocol {
    typealias InputActionType = AppAction // It wants to receive all possible app actions
    typealias OutputActionType = AppAction          // No action is generated from this Middleware
    typealias StateType = AppState
    
    // TODO: Seems wrong to keep subscriptions here
    var cancellables = [AnyCancellable]()
    
    func handle(action: AppAction, from dispatcher: ActionSource, state: @escaping GetState<AppState>) -> IO<AppAction> {
        switch action {
        case .loadNextPage:
//            print("will load page \(state().photosPage)")
            return onLoadNextPage(page: state().photosPage)
        case .addPhotos(_): return .pure()
        case .clearPhotos: return .pure()
        }
    }

    private func onLoadNextPage(page: Int) -> IO<AppAction> {
        IO { [weak self] output in
            guard let self = self else { return }
            do {
                try UnsplashAPI.shared
                    .fetchPhotos(page: page)
                    .subscribe(on: DispatchQueue.global(qos: .userInitiated))
                    .receive(on: RunLoop.main)
                    .sink(receiveCompletion: { _ in
//                        print("Received completion: \($0).")
                    }, receiveValue: { photos in
//                        print("Received photos:")
//                        print(photos)
                        output.dispatch(.addPhotos(photos: photos.map(\.toUIModel)))
                    })
                    .store(in: &self.cancellables)
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
