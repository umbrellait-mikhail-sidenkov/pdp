//
// Created by Mikhail Sidenkov on 20.12.2021.
//

import Foundation
import UIKit

protocol ImageCache {
    func getImageBy(url: String) -> UIImage?
    func saveImage(_ image: UIImage?, byUrl url: String)
}

class ImageNSCache: ImageCache {
    static let sharedCache = ImageNSCache()

    private let cachedImages = NSCache<NSString, UIImage>()

    func getImageBy(url: String) -> UIImage? {
        cachedImages.object(forKey: url as NSString)
    }

    func saveImage(_ image: UIImage?, byUrl url: String) {
        guard let image = image else { return }
        cachedImages.setObject(image, forKey: url as NSString)
    }
}