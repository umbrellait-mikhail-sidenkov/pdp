//
//  UnsplashAPI.swift
//  PDP
//
//  Created by Mikhail Sidenkov on 17.12.2021.
//

import Foundation
import Combine

struct UnsplashPhotoResponseDTO: Codable, Hashable {
    let id: String
    let urls: [String: String]

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    static func == (lhs: UnsplashPhotoResponseDTO, rhs: UnsplashPhotoResponseDTO) -> Bool {
        lhs.id == rhs.id
    }

    var toUIModel: UnsplashPhoto {
        UnsplashPhoto(thumbUrl: urls[UnsplashPhotoURL.small.rawValue]!,
                      regularUrl: urls[UnsplashPhotoURL.regular.rawValue]!)
    }
}

struct UnsplashPhoto: Hashable {
    let id = UUID()
    let thumbUrl: String
    let regularUrl: String
}

enum UnsplashPhotoURL: String {
    case raw, full, regular, small, thumb
}

class UnsplashAPI {

    static let shared = UnsplashAPI()
    
    private var cancellables = [AnyCancellable]()
    
    private static let baseURL = "https://api.unsplash.com"
    private static let pathPhotos = "/photos"
    private static let accessKey = "Client-ID D-mkFYlmHbWj4IuLRbVfYi-JRsobbCwNjiq6S3gyWLA"
    
    func fetchPhotos(page: Int) throws -> AnyPublisher<[UnsplashPhotoResponseDTO], Error> {
        guard var url = URLComponents(string: Self.baseURL + Self.pathPhotos) else {
            throw URLError(.badURL)
        }
        
        url.queryItems = [ URLQueryItem(name: "page", value: String(page)),
                           URLQueryItem(name: "per_page", value: "20")]

        guard let fullUrl = url.url else {
            throw URLError(.badURL)
        }

        var request = URLRequest(url: fullUrl)
        request.setValue(Self.accessKey, forHTTPHeaderField: "Authorization")

        return URLSession(configuration: .default)
            .dataTaskPublisher(for: request)
            .tryMap { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                
                if httpResponse.statusCode != 200 {
                    print("status code: \(httpResponse.statusCode)")
                    throw URLError(.resourceUnavailable)
                }
                return element.data
            }

            // TODO: ADD SNAKE CASE DECODING STRATEGY
        
            .decode(type: [UnsplashPhotoResponseDTO].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
