//
// Created by Mikhail Sidenkov on 21.12.2021.
//

import Foundation
import UIKit

protocol ImageRepository {
    func fetchAsync(url: String, completion: ((UIImage?) -> Void)?)
    func getImageBy(url: String, onCompleted: ((_ image: UIImage?) -> ())?)
    func getCachedImage(url: String) -> UIImage?
    func cancelFetch(_ identifier: String)
}

extension ImageRepository {
    func fetchAsync(url: String) {
        fetchAsync(url: url, completion: nil)
    }
}

class UnsplashImageRepository: ImageRepository {

    static let shared = UnsplashImageRepository(cache: ImageNSCache())

    private let api: UnsplashAPI
    private let cache: ImageCache

    init(cache: ImageCache, api: UnsplashAPI = .init()) {
        self.cache = cache
        self.api = api
    }

    private let serialAccessQueue = OperationQueue().then {
        $0.maxConcurrentOperationCount = 1
        $0.qualityOfService = .userInitiated
    }

    private let fetchQueue = OperationQueue().then {
        $0.qualityOfService = .userInitiated
    }

    private var completionHandlers = [NSString : [(UIImage?) -> Void]]()

    func fetchAsync(url: String, completion: ((UIImage?) -> Void)? = nil) {
//        print("fetch async")
        serialAccessQueue.addOperation {
//            print("    addOperation")
            if let completion = completion {
//                print("        completion adding")
                let handlers = self.completionHandlers[url as NSString, default: []]
                self.completionHandlers[url as NSString] = handlers + [completion]
            }

            self.fetchData(url: url)
        }
    }

    func cancelFetch(_ identifier: String) {
        serialAccessQueue.addOperation {
            self.fetchQueue.isSuspended = true
            defer {
                self.fetchQueue.isSuspended = false
            }

            self.getOperation(url: identifier)?.cancel()
            self.completionHandlers[identifier as NSString] = nil
        }
    }

    private func fetchData(url: String) {
        guard getOperation(url: url) == nil else { return }

        if let data = cache.getImageBy(url: url) {
            invokeCompletionHandlers(for: url as NSString, with: data)
        } else {
            let operation = LoadImageOperation(url: url)

            operation.completionBlock = { [weak operation] in
                guard let fetchedData = operation?.fetchedData else { return }
                self.cache.saveImage(fetchedData, byUrl: url)

                self.serialAccessQueue.addOperation {
                    self.invokeCompletionHandlers(for: url as NSString, with: fetchedData)
                }
            }

            fetchQueue.addOperation(operation)
        }
    }

    private func getOperation(url: String) -> LoadImageOperation? {
        for case let fetchOperation as LoadImageOperation in fetchQueue.operations
            where !fetchOperation.isCancelled && fetchOperation.urlString == url {
            return fetchOperation
        }

        return nil
    }


    private func invokeCompletionHandlers(for identifier: NSString, with fetchedData: UIImage) {
        let completionHandlers = completionHandlers[identifier, default: []]
//        print("        invoking ch, ch count \(completionHandlers.count)")
        self.completionHandlers[identifier] = nil

        for completionHandler in completionHandlers {
//            print("           ch for id \(identifier)")
            completionHandler(fetchedData)
        }
    }

    func getCachedImage(url: String) -> UIImage? {
        cache.getImageBy(url: url)
    }

    func getImageBy(url: String, onCompleted: ((_ image: UIImage?) -> ())? = nil) {
        if let image = cache.getImageBy(url: url) {
            onCompleted?(image)
        } else {
            DispatchQueue.global(qos: .userInteractive).async { [weak self] in
                do {
                    guard let urlForImage = URL(string: url) else { throw URLError(.badURL) }
                    let image = UIImage(data: try Data(contentsOf: urlForImage))
                    onCompleted?(image)
                    self?.cache.saveImage(image, byUrl: url)
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }
    }
}

class LoadImageOperation: Operation {
    let urlString: String

    private(set) var fetchedData: UIImage?

    init(url: String) {
        urlString = url
    }

    override func main() {
        if isCancelled { return }

        do {
            guard let url = URL(string: urlString) else { return }
            let image = UIImage(data: try Data(contentsOf: url))
            fetchedData = image
        } catch {
//             TODO handle error
            print("bad")
        }
    }
}
