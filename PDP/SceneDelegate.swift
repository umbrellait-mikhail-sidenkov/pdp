//
//  SceneDelegate.swift
//  PDP
//
//  Created by Mikhail Sidenkov on 16.12.2021.
//

import UIKit
import Swinject

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var coordinator: MainCoordinator?

    private func createSwinjectContainer() -> Container {
        let container = Container()

        container.register(ImageCache.self) { _ in ImageNSCache() }

        container.register(ImageRepository.self) { r in
            UnsplashImageRepository(cache: r.resolve(ImageCache.self)!)
        }.inObjectScope(.container)

        container.register(MainViewController.self) { (r, coordinator: MainCoordinator) in
            MainViewController(coordinator, imageRepo: r.resolve(ImageRepository.self)!)
        }

        container.register(PhotoVC.self) { (r, coordinator: MainCoordinator, photo: UnsplashPhoto) in
            PhotoVC(coordinator, photo: photo, imageRepo: r.resolve(ImageRepository.self)!)
        }

        return container
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }

        let container = createSwinjectContainer()
        let navController = UINavigationController()

        coordinator = MainCoordinator(navController: navController, container: container)
        coordinator?.start()
        
        window = UIWindow(windowScene: scene)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
}

